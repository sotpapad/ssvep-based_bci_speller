# SSVEP-based_BCI_speller

Comparison of Canonical Correlation Analysis (CCA) and Filter Bank Canonical Correlation Analysis (FBCCA) for online decoding of characters presented as a matrix on a LCD monitor.

The algorithms are inspired by:
- [Chen, X., Wang, Y., Gao, S., Jung, T. P. & Gao, X. Filter bank canonical correlation analysis for implementing a high-speed SSVEP-based brain-computer interface. J. Neural Eng. 12, 46008 (2015), DOI:10.1088/1741-2560/12/4/046008.](https://pubmed.ncbi.nlm.nih.gov/26035476/)
- [Wang, Y., Chen, X., Gao, X. & Gao, S. A Benchmark Dataset for SSVEP-Based Brain-Computer Interfaces. IEEE Trans. Neural Syst. Rehabil. Eng. 25, 1746–1752 (2017), DOI:10.1109/TNSRE.2016.2627556.](https://pubmed.ncbi.nlm.nih.gov/27849543/)

Benchmark datasets accompanying the later research article are freeely available at: http://bci.med.tsinghua.edu.cn/download.html.
