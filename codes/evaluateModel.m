%% Function that computes the accuracy of the model on the test set

% Inputs:
%        model: trained model using all the training data and the
%               bestparameters
%       X_test: (L x M) matrix; L is the number of samples and M is the
%                number of variables
%       Y_test: (L x 1) vector; L is the number of samples

% Outputs:
%         accuracy - accuracy of the model on the test set

function [accuracy] = evaluateModel(model, X_test, Y_test)
    % make a prediction based on the model that comes from modelSelection_CV.m
    predictions = predict(model,X_test);
    
    % in the case that TreeBagger is selected as the best model, then the
    % predict function returns a cell array of character vectors
    if iscell(predictions)
        % if that is the case, covnert characters to double
        predictions = str2double(predictions);
    end
    
    % compute the accuracy
    accuracy = length(find(predictions==Y_test))/length(Y_test);
end
