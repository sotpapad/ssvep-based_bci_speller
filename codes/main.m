%% 1. Data extraction. 
% Assuming that you have downloaded the benchmark dataset and you are in the directory of the data, load them
% sequentially and keep only the electrodes we are interested in, i.e. those related to the occipital cortex.

% Initialize a cell array to store all relevant data.
all_subjects = cell(1,35);

% Electrodes (according to accompanying refernce files).
electr = [48, 54, 55, 56, 57, 58, 61, 62, 63];

% Number of subject.
numbers = 1:1:35;

for n=1:35
    % create filename
    subject = strcat("S",num2str(numbers(n)));
    name = strcat(subject,".mat");
    
    % load file
    load(name);
    
    % save data:
    % We discard the first 0.5s as it relates to the cue presentation, and
    % the characters are not yet flickering.
    all_subjects{1,n} = data(electr,125:1500,:,:);   
end

%% 2. Hyperparameters declaration

% Number of targets: instead of using the "known frequency" of a charecter as index,
% we will instead assign a unique value (1-40) to each character. This is valid since
% the task is repeated the same way for all trials, blocks and subjects.
k = 1:1:40;

% Sampling frequency.
fs = 250;

% Number of harmonics.
N = 5;

% Number of filter banks.
fb = 10;

% Frequencies of targets.
fr = 8.0:0.2:15.8;

% Time lengths: we need to discard the last 0.5s (originally 1375:1500 -> now 1250:1375)
% as it relates to the inter-trial resting period.
time_step = floor(250/4);   % 1s = 250 time points
time = 1:time_step:1250;

% All possible reference times and corresponding reference signals.
ref_time = cell(1,length(time)-1);
Y = cell(max(k),length(time)-1);

for t=2:length(time)
    % various time lengths
    ref_time{1,t-1} = 1:1:time(t);
    for fk=1:max(k)
        % up to N harmonics in the reference signals
        Yfk = nan(2*N,length(ref_time{1,t-1}));
        for n=1:N
            Yfk(2*n-1,:) = sin(2*pi*n*fr(fk).*ref_time{1,t-1}/fs);
            Yfk(2*n,:) = cos(2*pi*n*fr(fk).*ref_time{1,t-1}/fs);
        end
        % various reference signals
        Y{fk,t-1} = Yfk;
    end
end

%% 3. CCA - targets extraction.
% For each subject select a time length(1-21), a trial(1-6) and a
% target(1-40), take all relevant recordings, compute CCA, and proceed.

% Design a band pass filter [7-90 Hz], to be used before CCA.
bpFilt = designfilt('bandpassiir','FilterOrder',2, ...
        'PassbandFrequency1',7,'PassbandFrequency2',90, ...
        'PassbandRipple',3,'SampleRate',250);

% Accuracies per time length initialization.
targets_per_subj = cell(1,length(all_subjects));

% for all subjects
for sub=1:35           % 1-8 are experienced
    % choose subject
    subj = all_subjects{1,sub};
    
    % total targets computed for each subject (trials x time lengths x
    % blocks)
    targets_per_subj{1,sub} = nan(40,length(ref_time)+1,6);
    % last column corresponds to target number (== number of row)
    for r=1:6
        targets_per_subj{1,sub}(:,end,r) = k;
    end
        
    % choose time length
    for tl=1:length(ref_time)
        
        % choose block of trials
        for tr=1:6
            
            % choose SSVEP
            for tar=1:40
                % SSVEP - band passed
                x = filtfilt(bpFilt, subj(:,ref_time{1,tl},tar,tr).' );
                X = x.';
                % perform CCΑ between SSVEPS and all targets
                [index] = CCA(X,Y,tl);
                % save estimated target frequency index
                targets_per_subj{1,sub}(tar,tl,tr) = index;           
            end     
        end
    end
end

%% 4. Classifier CCA input generation.

% Split data into experienced and naive users, and merge each catecory in one big matrix.
exper_cca = nan(40*6*8,length(ref_time)+1);
naive_cca = nan(40*6*27,length(ref_time)+1);

for sub=1:35
    if sub<=8
        exper_subj = nan(40*6,length(ref_time)+1);
        for bl=1:6
            exper_subj(1+40*(bl-1):40+40*(bl-1),:) = targets_per_subj{1,sub}(:,:,bl);
        end
        exper_cca(1+40*6*(sub-1):40*6+40*6*(sub-1),:) = exper_subj;
    else
        naive_subj = nan(40*6,length(ref_time)+1);
        for bl=1:6
            naive_subj(1+40*(bl-1):40+40*(bl-1),:) = targets_per_subj{1,sub}(:,:,bl);
        end
        naive_cca(1+40*6*(sub-9):40*6+40*6*(sub-9),:) = naive_subj;
    end
end

% Save generated data relating to CCA.
save('cca_targets', 'exper_cca', 'naive_cca');

%% 5. Repeated CV on CCA generated data.

% Number of repetitions.
num_reps = 10;

% Initialize arrays to store the estimated accuracies.
exper_cca_accs = nan(num_reps,length(ref_time));
naive_cca_accs = nan(num_reps,length(ref_time));
all_cca_accs = nan(num_reps,length(ref_time));

% Avoid "parfor" for broadcast variables.
exper_cca_data = cell(1,length(ref_time));
naive_cca_data = cell(1,length(ref_time));
for ii=1:length(ref_time)
    exper_cca_data{1,ii} = [exper_cca(:,ii), exper_cca(:,end)];
    naive_cca_data{1,ii} = [naive_cca(:,ii), naive_cca(:,end)];
end

% For each time length perform nested CV and estimate accuracy.
parfor tl=1:length(ref_time)
    % experienced subjects
    [exper_acc] = CV(exper_cca_data{1,tl},num_reps);
    exper_cca_accs(:,tl) = exper_acc;
    % naive subjects
    [naive_acc] = CV(naive_cca_data{1,tl},num_reps);
    naive_cca_accs(:,tl) = naive_acc;
    % all subjects
    all_cca_data = vertcat(exper_cca_data{1,tl},naive_cca_data{1,tl});
    [all_acc] = CV(all_cca_data,num_reps);
    all_cca_accs(:,tl) = all_acc;
    
    fprintf(strcat('Finished CCA iteration no: ', num2str(tl)));
    fprintf('\n');
end

% Save generated decoding accuracies relating to CCA.
save('CCA_accuracies','exper_cca_accs', 'naive_cca_accs', 'all_cca_accs');

%% 6. FBCCA - targets extraction.
% For each subject select a time length(1-21), a trial(1-6) and a
% target(1-40), take all relevant recordings, compute FBCCA, and proceed.

% In Chen et al, 2015, they have found that the best parameters for
% the previous to last of FBCCA step are a=1.25, b=0.25, N=7.
a = 1.25;
b = 0.25;
N = 7;

% Chebysev type I IIR filters design with M3 method (Chen et al, 2015).
% Design multiple band pass filters [7*N-90 Hz], to be used before FBCCA.
filters = cell(1,N);
for freq=1:N
    bpFilt = designfilt('bandpassiir','FilterOrder',2, ...
        'PassbandFrequency1',freq*8,'PassbandFrequency2',88, ...
        'PassbandRipple',3,'SampleRate',250);
    filters{1,freq} = bpFilt;
end

% Accuracies per time length initialization.
targets_per_subj_fb = cell(1,length(all_subjects));

% for all subjects
for sub=1:35           % 1-8 are experienced
    % choose subject
    subj = all_subjects{1,sub};
    
    % total targets computed for each subject (trials x time lengths x
    % blocks)
    targets_per_subj_fb{1,sub} = nan(40,length(ref_time)+1,6);
    % last column corresponds to target number (== number of row)
    for r=1:6
        targets_per_subj_fb{1,sub}(:,end,r) = k;
    end
        
    % choose time length
    for tl=1:length(ref_time)
        
        % choose block of trials
        for tr=1:6
            
            % choose SSVEP
            for tar=1:40
                % SSVEP - band passed
                X = filtfilt(bpFilt, subj(:,ref_time{1,tl},tar,tr).' );
                %X = x.';
                % perform FBCCA between SSVEPS and all targets
                [index] = FBCCA(X,Y,tl,filters,a,b);
                % save estimated target frequency index
                targets_per_subj_fb{1,sub}(tar,tl,tr) = index;           
            end     
        end
    end
fprintf(strcat('Done subject: ',num2str(sub)));
end

%% 7. Classifier FBCCA input generation.

% Split data to experienced and naive users, and merge each category in one big matrix.
exper_fbcca = nan(40*6*8,length(ref_time)+1);
naive_fbcca = nan(40*6*27,length(ref_time)+1);

for sub=1:35
    if sub<=8
        exper_subj = nan(40*6,length(ref_time)+1);
        for bl=1:6
            exper_subj(1+40*(bl-1):40+40*(bl-1),:) = targets_per_subj{1,sub}(:,:,bl);
        end
        exper_fbcca(1+40*6*(sub-1):40*6+40*6*(sub-1),:) = exper_subj;
    else
        naive_subj = nan(40*6,length(ref_time)+1);
        for bl=1:6
            naive_subj(1+40*(bl-1):40+40*(bl-1),:) = targets_per_subj{1,sub}(:,:,bl);
        end
        naive_fbcca(1+40*6*(sub-9):40*6+40*6*(sub-9),:) = naive_subj;
    end
end

% Save generated data relating to FBCCA.
save('fbcca_targets', 'exper_fbcca', 'naive_fbcca');

%% 8. Repeated CV on FBCCA generated data.

% Number of repetitions.
num_reps = 10;

% Initialize arrays to store the estimated accuracies.
exper_fbcca_accs = nan(num_reps,length(ref_time));
naive_fbcca_accs = nan(num_reps,length(ref_time));
all_fbcca_accs = nan(num_reps,length(ref_time));

% Avoid "parfor" for broadcast variables.
exper_fbcca_data = cell(1,length(ref_time));
naive_fbcca_data = cell(1,length(ref_time));
for ii=1:length(ref_time)
    exper_fbcca_data{1,ii} = [exper_fbcca(:,ii), exper_fbcca(:,end)];
    naive_fbcca_data{1,ii} = [naive_fbcca(:,ii), naive_fbcca(:,end)];
end

% For each time length perform nested CV and estimate accuracy.
parfor tl=1:length(ref_time)
    % experienced subjects
    [exper_acc] = CV(exper_fbcca_data{1,tl},num_reps);
    exper_fbcca_accs(:,tl) = exper_acc;
    % naive subjects
    [naive_acc] = CV(naive_fbcca_data{1,tl},num_reps);
    naive_fbcca_accs(:,tl) = naive_acc;
    % all subjects
    all_fbcca_data = vertcat(exper_fbcca_data{1,tl},naive_fbcca_data{1,tl});
    [all_acc] = CV(all_fbcca_data,num_reps);
    all_fbcca_accs(:,tl) = all_acc;
    
    fprintf(strcat('Finished FBCCA iteration no: ', num2str(tl)));
    fprintf('\n');
end

% Save generated decoding accuracies relating to FBCCA.
save('FBCCA_accuracies','exper_fbcca_accs', 'naive_fbcca_accs', 'all_fbcca_accs');

%% Plot of Results

% Choose 1 of 5 plots to create (see below).
flag = 5;

if flag == 1
    data = cell(1,2);
    data{1,1} = all_cca_accs;
    data{1,2} = all_fbcca_accs;
    type = 'all';
    plot_data(data,type);
elseif flag == 2
    data = cell(1,2);
    data{1,1} = exper_cca_accs;
    data{1,2} = naive_cca_accs;
    type = 'CCA: exprerienced vs naive';
    plot_data(data,type);
elseif flag == 3
    data = cell(1,2);
    data{1,1} = exper_fbcca_accs;
    data{1,2} = naive_fbcca_accs;
    type = 'FBCCA: exprerienced vs naive';
    plot_data(data,type); 
elseif flag == 4
    data = cell(1,2);
    data{1,1} = exper_cca_accs;
    data{1,2} = exper_fbcca_accs;
    type = 'CCA vs FBCCA: experienced subjects';
    plot_data(data,type);
elseif flag == 5
    data = cell(1,2);
    data{1,1} = naive_cca_accs;
    data{1,2} = naive_fbcca_accs;
    type = 'CCA vs FBCCA: naive subjects';
    plot_data(data,type);
end
