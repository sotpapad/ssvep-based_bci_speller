%% Function that splits the dataset in train-test partitions

% Inputs:
%        A: (N x M) matrix; N is the number of samples and M is the number
%           of variables
%        percentage: float that dictates the percentage of samples that
%                    should be used for training

% Outputs:
%        train_set: training set
%        test_set: test set

function [train_set,test_set] = random_split(A,percentage)
    % 70% can not always be integer, so use round
    J = size(A,1);
    split = round(percentage*J);

    % randomly pick split % of the samples, make them the training set and use
    % the rest as the test set
    [train_set, ids_train] = datasample(A,split,1,'Replace',false);
    ids_test = setdiff(1:J,ids_train);
    test_set = A(ids_test,:);
end
