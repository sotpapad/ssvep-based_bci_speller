%% Function that computes FBCCA for a single trial and a given time length

% Inputs:
%        X: recordings from 9 channels for all targets
%        Y: reference signal for all targets
%        tl: time length of data used
%        filters: Chebysev type I filters for sub-bands generation
%        a: parameter needed for correlation maximization
%        b: parameter needed for correlation maximization

% Outputs:
%        index: index of maximum correlation

function [index] = FBCCA(X,Y,tl,filters,a,b)

    % preallocation of array to store correlation values between one X and
    % all 40 Y
    all_ro = nan(1,40);

    % computation of all correlations
    for fk=1:40
        y = Y{fk,tl};
        % preallocation of array that stores all correlations between a
        % reference signal and all sub-bands
        sb_ro = nan(1,length(filters));
        weights = nan(1,length(filters));
        % compare all sub-bands to each reference signal
        for sb=1:length(filters)
            Xsb = filtfilt(filters{1,sb},X);
            [~,~,r] = canoncorr(Xsb,y.');
            % keep max eigenvalues
            sb_ro(1,sb) = r(1);
            % compute corresponding weight
            weights(1,sb) = sb^(-a)+b;
        end
        % calculation of feature for target extraction
        rok = sum(weights.*(sb_ro).^2);
        all_ro(1,fk) = rok;
    end
  
   % get the index of choosen target (the max of max eigenvalues)
   [~,index] = max(all_ro);
    
end
