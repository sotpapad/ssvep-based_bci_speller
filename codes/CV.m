%% Function that implements a repeated cross-validation

% Inputs:
%        data: data fed to the classifer(s) - the expected strusture is
%              rows representing samples, and columns representing
%              variables, except for the last one, which should be the
%              target values
%        num_reps: number of repetitions of the CV procedure

% Outputs:
%        model_accuracy: accuracy of the model trained on the training set,
%                        upon presentation of the test set

function [model_accuracy] = CV(data,num_reps)

    % initialize array to store accuracies
    model_accuracy = nan(num_reps,1);

    % repetitions of algorithm
    reps = num_reps;

    % folds to be used for spliting the data
    k = 10;

    for r=1:reps
        % shuffle the samples in the data
        data = data(randperm(size(data,1)),:);
       
        % split in training and test sets (70-30)
        [train_set, test_set] = random_split(data,0.7);

        % split each set in X and Y
        train_X = train_set(:,1:end-1);
        train_Y = train_set(:,end);
        test_X = test_set(:,1:end-1);
        test_Y = test_set(:,end);

        % performance estimation
        % 1. cross-validation on training data
        [model, ~, ~] = modelSelection_CV(train_X, train_Y, k);

        % 2. accuracy estimation on test set
        [accuracy] = evaluateModel(model, test_X,test_Y);
        model_accuracy(r,1) = accuracy;
    
    end
end
