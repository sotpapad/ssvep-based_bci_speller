% Function that creates plot of any combination of the variables

% Inputs:
%        data: cell array containing all the data to be plotted (different
%              data are expected to be contained in different arrays)
%        type: string describing the data that are compared in a plot

% Output:
%        plot of data

function [] = plot_data(data, type)

    % get the number of data to be plotted
    n = length(data);
    
    % x-axis
    x = 1:1:20;
    
    % create the figure
    figure;
    
    % get the default color order of matlab
    order = get(gca,'ColorOrder');
    
    % iteratively plot the data
    for d=1:n
        % compute mean and sem of data
        m = mean(data{1,d}).*100;
        s = std(data{1,d})./sqrt(size(data{1,d},1)).*100;
        % mean accuracy per time length
        plot(x,m);
        hold on;
        % sem of mean accuracies
        patch1 = fill([x fliplr(x)], [m, fliplr(m + s)], order(d));
        patch2 = fill([x fliplr(x)], [m, fliplr(m - s)], order(d));
        set(patch1, 'edgecolor', 'none');
        set(patch1, 'FaceAlpha', 0.25);
        set(patch2, 'edgecolor', 'none');
        set(patch2, 'FaceAlpha', 0.25);
        % do not display sem fills area in legend
        set(get(get(patch1(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
        set(get(get(patch2(1),'Annotation'),'LegendInformation'),'IconDisplayStyle','off');
        hold on;
    end
    
    % use grid
    grid on;
    
    % axis labels and ticks
    xlabel('Time length (s)');
    ylabel('Accuracy (%)');
    
    ylim([0 100]);
    
    ax = gca;
    %ax.Ytick = 0:10:100;
    set(ax, 'xtick', 0:4:20);
    set(ax, 'xticklabels', 0:1:5);
    
    % title and legend
    if strcmpi(type, 'all')
        title('CCA vs FBCCA: all subjects');
        legend('CCA', 'FBCCA', 'Location', 'northwest');
        legend('boxoff');
    elseif strcmpi(type, 'CCA: exprerienced vs naive')
        title(type);
        legend('experienced', 'naive', 'Location', 'northwest');
        legend('boxoff');
    elseif strcmpi(type, 'FBCCA: exprerienced vs naive')
        title(type);
        legend('experienced', 'naive', 'Location', 'northwest');
        legend('boxoff');
    elseif strcmpi(type, 'CCA vs FBCCA: experienced subjects')
        title(type);
        legend('CCA', 'FBCCA', 'Location', 'northwest');
        legend('boxoff');
    elseif strcmpi(type, 'CCA vs FBCCA: naive subjects')
        title(type);
        legend('CCA', 'FBCCA', 'Location', 'northwest');
        legend('boxoff');
    % TODO: possibly other custom comparisons
    end
    
end