%% Function that computes CCA for a single trial and a given time length

% Inputs:
%        X: recordings from 9 channels for all targets
%        Y: reference signal for all targets
%        tl: time length of data used

% Outputs:
%        index: index of maximum correlation

function [index] = CCA(X,Y,tl)

    % preallocation of array to store correlation values between one X and
    % all 40 Y
    all_ro = nan(1,40);

    % computation of all correlations
    for fk=1:40
        y = Y{fk,tl};
        [~,~,r] = canoncorr(X.',y.');
        % keep max eigenvalues
        all_ro(1,fk) = r(1);
    end
  
   % get the index of choosen target (the max of max eigenvalues)
   [~,index] = max(all_ro);
    
end
