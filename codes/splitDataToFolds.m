%% Function that splits the data into k (approximately) equally sized folds

% Inputs:
%        X: (N x M) matrix; N is the number of samples and M is the number
%           of variables
%        Y: (N x 1) vector; the target class of each sample
%        k: scalar; the number of desired folds to be created

% Outputs:
%        folds: cell array of k matrices of size approximately (N/k x M)
%               folds

function [folds] = splitDataToFolds(X, Y, k)
    % join the inputs X and Y
    data = [X Y];
    
    % get the number of samples
    N = size(X,1);
    
    % choose N/k samples from the data matrix; since N/k can be float,
    % round the number of samples to the nearest integer
    split = round(N/k);
    
    % initialize the cell of k matrices
    folds = cell(k,1);
    
    % initialize a count in order to correctly insert the samples into the
    % folds cell array
    count = 0;
    
    % split the data in k folds and fill in the folds cell array
    for i=1:k-1
        % all folds up to k-1 are filled with a matrix with exactly split
        % amount of samples
        folds{i,1} = data(1+count:split+count,:);
        
        % inceremnt count by split amount
        count = count + split;
    end
    
    % the last fold will just contain all the samples that are left out by
    % the loop; depending on how split is rounded it can contain exactly
    % split amount of samples, less than split or more than split
    folds{end,1} = data(count+1:end,:);

end
