%% Function that selects the best model and parameters using a k-fold Cross
% Validation

% Inputs:
%        X: (N x M) matrix; N is the number of samples and M is the number
%           of variables
%        Y: (N x 1) vector; the target class of each sample
%        k: scalar; the number of desired folds to be created

% Outputs:
%       model: trained model using all the training (or provided) data and
%              the bestparameters
%       bestparameters: model description

function [model, model_type, train_accuracies] = modelSelection_CV(X, Y, k)
    % initialization of best parameters
    model_type = cell(1,1);
    
    % define the classifiers that will be used and the "code" that will be
    % returned in bestparameters
    % 11: Naive Bayes - Uniform Prior
    % 12: Naive Bayes - Empirical Prior
    % 21: Random Forest with 1000 trees - MinLeafSize == 1
    % 22: Random Forest with 1000 trees - MinLeafSize == 3
    % 23: Random Forest with 1000 trees - MinLeafSize == 5
    % 31: KNN - K == 5
    % 32: KNN - K == 15
    % 33: KNN - K == 25
    % 41: Default MATLAB Multi-class SVM
    
    % due to computational cost, I have choosen to perform only multiclass
    % SVM
    %classifiers = [11, 12, 21, 22, 23, 31, 32, 33, 41];
    classifiers = 41;
    
    % split data to k folds
    [folds] = splitDataToFolds(X, Y, k);
    
    % initialization of cell array that stores the different (k) training and
    % tune sets
    train_set = cell(k-1,k);
    tune_set = cell(1,k);
    
    % leave one fold out, as it should be the tune set; the rest will be
    % used as the training set (in case the algorithm is called not for
    % performance estimation, but rather for picking the best model, the
    % same logic applies; only this time we split in train and test)
    for i=1:k
        temp_folds = folds;
        tune_set{1,i} = temp_folds{i,1};
        temp_folds(i) = [];
        for j=1:k-1
            train_set{j,i} = temp_folds{j};
        end
    end
    
    % initialization of matrix that stores the accuracy of each configuration
    % on the training sets
    train_accuracies = zeros(1,length(classifiers));
    
    % train each configuration on each training set and get the accuracy by
    % testing the model on the respective tune set               
    for c=1:length(classifiers)
        % initialize a vector for the accuracy on each fold
        acc = zeros(k,1);

        for f=1:k
            % convert the cells into matrices and retrieve the class labels
            train_XY = cell2mat(train_set(:,f));
            train_Y = train_XY(:,end);
            train_X = train_XY(:,1:end-1);

            tune_XY = cell2mat(tune_set(1,f));
            tune_Y = tune_XY(:,end);
            tune_X = tune_XY(:,1:end-1);

            if c ~= length(classifiers)
                % train on the training set of a specific fold and threshold
                % with each classifier (see line 137)
                Mdl = fit(train_X, train_Y, classifiers(c));
                % estimate the accuracy on this fold and save it
                acc(f,1) = evaluateModel(Mdl, tune_X, tune_Y);
            else
                Mdl = fitcecoc(train_X, train_Y);
                acc(f,1) = evaluateModel(Mdl, tune_X, tune_Y);
            end    
        end

        % estimate the mean accuracy and save it
        accuracy = mean(acc);
        train_accuracies(1,c) = accuracy;   
    end
    
    % choose the best accuracy and return the bestparameters
    [~,index] = max(train_accuracies(:));
    model_type{1,1} = classifiers(1, index);
    
    % pick the configuration with the highest accuracy and train it on the
    % whole train-tune set; return the model
    model_X = X;
    model_Y = Y;
    if index ~= length(classifiers)
        model = fit(model_X, model_Y, classifiers(1, index));
    else
        model = fitcecoc(model_X, model_Y);
    end

end

%% Function that returns a model for each classifier's training
function [Mdl] = fit(X, Y, classifier)
    % use each of the predifined classifiers in order to produce a model
    % all the predictors are categorical
    if classifier==11
        % Naive Bayes - Uniform Prior
        Mdl = fitcnb(X,Y,'Prior','uniform');
    elseif classifier==12
        % Naive Bayes - Empirical Prior
        Mdl = fitcnb(X,Y,'Prior','empirical');
    elseif classifier==21
        % Random Forest with 1000 trees - MinLeafSize == 1
        Mdl = TreeBagger(100,X,Y,'MinLeafSize',1);
    elseif classifier==22
        % Random Forest with 1000 trees - MinLeafSize == 3
        Mdl = TreeBagger(1000,X,Y,'MinLeafSize',1);
    elseif classifier==23
        % Random Forest with 1000 trees - MinLeafSize == 5
        Mdl = TreeBagger(1000,X,Y,'MinLeafSize',5);
    elseif classifier==31
        % KNN - K == 5
        Mdl = fitcknn(X,Y,'NumNeighbors',5);
    elseif classifier==32
        % KNN - K == 15
        Mdl = fitcknn(X,Y,'NumNeighbors',15);
    elseif classifier==33
        % KNN - K == 25
        Mdl = fitcknn(X,Y,'NumNeighbors',25);
    end
end
